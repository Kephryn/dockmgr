#! /bin/bash

chmod -R 755 /var/www/html/camp-dock/cgi-bin
chown -R www-data:www-data /var/www/html/camp-dock/cgi-bin
chown www-data:www-data /opt/php/php-5.5.38/var/run/php-fpm.sock
chown www-data:www-data /opt/php/php-5.6.29/var/run/php-fpm.sock
chown www-data:www-data /opt/php/php-7.0.14/var/run/php-fpm.sock
chown www-data:www-data /opt/php/php-7.1.0/var/run/php-fpm.sock
mysql --execute "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';"