Camp Dock or Docker Camp or DockClamp
==

Docker Linux (Debian Jessie)

* CGI
* Apache  2.4
* MySQL   5.5
* Node.js 7.0

### Interpreters via CGI
* PHP    5.6, 7.0
* HHVM
* Ruby
* Perl
* Python

### CGI -> Node.js Cross Compilers
* markdown
* sass
* less
* haml
* pug
* coffee

### HTML Template Engines
These template engines should be looked into, try to support as many of them as possible and try to make them easy to implement, add and remove.

* atpl
* dust
* eco
* ect
* ejs
* haml
* haml-coffee
* handlebars
* hogan
* jade
* jazz
* jqtpl
* just
* liquor
* mustache
* qejs
* swig
* templayed
* toffee
* underscore
* walrus
* whiskers

PHP Stuff
==
Setup Adminer wget https://www.adminer.org/latest-mysql-en.php, make it look nice and integrate it via iframe for use in dashboard.

List of things I need to get done
==
* Make the databases store locally, so they are not lost if the server is rebuilt.
* Add Adminer to shadow files
