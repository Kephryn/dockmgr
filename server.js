var express = require('express'),
	app = express();

var SSE = require('express-sse');
var sse = new SSE(['asdf'], {path: '/stream'});

var services = [
	{
		name: 'mysql',
		cmd: 'mysql --execute "GRANT ALL PRIVILEGES ON *.* TO \'root\'@\'%\';"',
	},
];



app.get('/stream', sse.init);

setInterval(function(){
	sse.send('apples');
	sse.send(['oranges'], 'system-statistic');
}, 10000);

app.get('/status', function (req, res) {
	var statuses = [];
	services.forEach(function(service) {
		statuses.push({
			name: service.name,
			status: service.status
		});
	});

	res.send(JSON.stringify(statuses));
});

// app.get('/services', function (req, res) {
// 	res.send(JSON.stringify(services));
// });

// services.forEach(function(index, item){
// 	var service = item;
// 	console.log('/services/' + service.name)
// 	app.get('/services/' + service.name, function (req, res) {
// 		res.send(JSON.stringify(services[service.name]));
// 	});
// });


app.use(express.static(__dirname + '/public'));

var server = app.listen(8091, function () {
	var host = server.address().address;
	var port = server.address().port;


	console.log('Example app listening at http://%s:%s', host, port);
});