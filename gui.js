var express = require('express')
  , app = express()
  , serverEvent = require('server-event')
  // serverEvent = serverEvent({ express : app }); // Optional. Pass in reference to express to access client.js file from client side 

  app.get('/events', serverEvent, function (req, res) {
      res.sse('test', "event with name test");
      res.sse('default event name message');
  });

app.get('/test', function (req, res) {
  res.send('Hello World! Bitch! Yes!');
});

app.use(express.static('html5-boilerplate/dist'));

var server = app.listen(8090, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
