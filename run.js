//Service script, node management console for managing internal services and settings.
var fs = require('fs');
var services = [
		{
			name: 'apache2',
			// cmd: 'apachectl start',
			start: 'service apache2 start',
			kill: 'service apache2 stop',
		},
		{
			name: 'php-fpm',
			start: 'service php5-fpm start',
			kill: 'service php5-fpm stop'
		},
		// {
		// 	name: 'hhvm',
		// 	start: 'hhvm --mode server -d hhvm.server.type=fastcgi -d hhvm.server.port=9000 -d hhvm.server.fix_path_info=true'
		// 	// cmd: 'hhvm --mode server -vServer.Type=fastcgi -vServer.FileSocket=/var/run/hhvm/sock'
		// },
		// {
		// 	name: 'php55',
		// 	start: '/opt/php/php-5.5.38/sbin/php-fpm --fpm-config /opt/php/php-5.5.38/etc/php-fpm.conf'
		// },
		// {
		// 	name: 'php56',
		// 	start: '/opt/php/php-5.6.29/sbin/php-fpm --fpm-config /opt/php/php-5.6.29/etc/php-fpm.conf'
		// },
		// {
		// 	name: 'php70',
		// 	start: '/opt/php/php-7.0.14/sbin/php-fpm --fpm-config /opt/php/php-7.0.14/etc/php-fpm.conf'
		// },
		// {
		// 	name: 'php71',
		// 	start: '/opt/php/php-7.1.0/sbin/php-fpm --fpm-config /opt/php/php-7.1.0/etc/php-fpm.conf'
		// },
		// {
		// 	name: 'mysqld',
		// 	// cmd: 'service mysqld start',
		// 	cmd: '/usr/bin/mysqld_safe',
		// 	on: {
		// 		up: function() {
		// 			fs.unlink('./firstrun', function (err) {
		// 				if (err) throw err;
		// 				console.log('successfully deleted /tmp/hello');
		// 			});
		// 		},
		// 		down: function() {

		// 		}
		// 	}
		// },
	]
	, async = require('async')
	, nodemon = require('nodemon')
	, child_process = require('child_process')
	, express = require('express')
	, app = express()
	, SSE = require('sse')
	, sse = new SSE(app, {path: '/services'})
	, clients = []
	, messages = {}

	// , forever = require('forever-monitor')
	// , child = new (forever.Monitor)('gui.js')
	// child.on('watch:restart', function(info) {
	//     console.error('Restaring script because ' + info.file + ' changed');
	// });
	//
	// child.on('restart', function() {
	//     console.error('Forever restarting script for ' + child.times + ' time');
	// });
	//
	// child.on('exit:code', function(code) {
	//     console.error('Forever detected script exited with code ' + code);
	// });

	// child.start();

	services.forEach(function(service) {
		console.log(service.name);
		service.status = false;
		messages[service.name] = [];

		// Object.observe(service, function(changes) {
		// 	console.log("Changes: ", changes);

		// 	clients.forEach(function(client){
		// 		client.send('status', {
		// 			name: changes.name,
		// 			status: changes.object[changes.name]
		// 		});
		// 	});
		// });

		if(service.start) {
			if(typeof(service.start) == 'string')
				service.start = [service.start];

			service.start.forEach(function(command) {
				child = child_process.exec(command, function (error, stdout, stderr) {
					messages[service.name].push(stdout);
					console.log('stdout: ' + stdout);

					if(stderr) {
						console.log('stderr: ' + stderr);
					}

					if (error !== null) {
						console.log('exec error: ' + error);
					}
				});
			});
		}

		if(service.file) {
			child = child_process.execFile(service.file, function (error, stdout, stderr) {
				console.log('stdout: ' + stdout);
				if(stderr) {
					console.log('stderr: ' + stderr);
				}
				if (error !== null) {
					console.log('exec error: ' + error);
				}
			});
		}

		setInterval(function(){
			var	monitor = child_process.exec('ps ax', function (error, stdout, stderr) {

				services.forEach(function(service) {
					service.status = stdout.indexOf(service.name) != -1;
					// console.log(service.name + ' is ' + (service.status? 'Up': 'Down'));
				});

				if(stderr) {
					console.log('stderr: ' + stderr);
				}
				if (error !== null) {
					console.log('exec error: ' + error);
				}
			});
		}, 2000);

	});

	sse.on('connection', function(client) {
		console.log(client)
		client.id = clients.length;
		clients.push(client);

		client.on('close', function() {
			clients.splice(client.id, 1);
		})

		services.forEach(function(service) {
			client.send('test');
		});
	});

	app.get('/status', function (req, res) {
		var statuses = [];
		services.forEach(function(service) {
			statuses.push({
				name: service.name,
				status: service.status
			});
		});

		res.send(JSON.stringify(statuses));
	});

	app.get('/services', function (req, res) {
		res.send(JSON.stringify(services));
	});

	services.forEach(function(index, item){
		var service = item;
		console.log('/services/' + service.name)
		app.get('/services/' + service.name, function (req, res) {
			res.send(JSON.stringify(services[service.name]));
		});
	});


	app.use(express.static(__dirname + '/public'));

	var server = app.listen(8090, function () {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log('Example app listening at http://%s:%s', host, port);
	});

	setTimeout(function(){
		child_process.exec('bash ./firstrun.sh');
	}, 10000);
